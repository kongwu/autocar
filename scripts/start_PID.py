#!/usr/bin/env python
'''
This script first initiates a moving forward command, getting the yaw angle feedback from
the AutoCar, publish it on the 'feedback' topic,
'''
import rospy
from std_msgs.msg import String
from autocar.msg import Odom
import numpy as np
import math
import time
from autocar_PID import stepsize
import serial
from xbee import XBee
from receive_cmd import feedback_xbee
import struct

xbee = None
# xbee address DH + DL concatenated
XBEE_ADDR_LONG = '\x00\x00\x00\x00\x00\x00\x00\x00'
# xbee 16-bit Source Address MY
XBEE_ADDR_SHORT = '\x00\x01'

# change your port here!
PORT = '/dev/ttyUSB0'

pub = rospy.Publisher('feedback', Odom, queue_size=100)

#format the command such that Fio can decode
def cmd_format(cmd, target):
    if abs(target) < 10:
        final = str(cmd) + '00' + str(target)
    if 10 <= abs(target) < 100:
        final = str(cmd) + '0' + str(target)
    if abs(target) >= 100:
        final = str(cmd) + str(target)
    return final


def callback(fb_cmd):

    cmd = cmd_format(fb_cmd.cmd, fb_cmd.target)
    # log infor using rospy, preparing to send via XBee to Fio
    rospy.loginfo("Sending command to Fio: %s" % cmd)
    # send via xbee
    xbee.tx(dest_addr_long=XBEE_ADDR_LONG, dest_addr=XBEE_ADDR_SHORT, data=cmd, )
    feedback_xbee()


#waits for feedback from Fio via XBee, decode the feedback, and publish back to the master node
def feedback_xbee():
    #wait and read the reply from Fio
    response = xbee.wait_read_frame()
    #decode the feedback message
    cmd = response['rf_data'][0]
    target = struct.unpack('>h', response['rf_data'][1:3])[0]
    fback = struct.unpack('>h', response['rf_data'][-2:])[0]
    #declare message type, defined in Odom
    msg = Odom()
    #direction command, 'w','s','a', or 'd'
    msg.cmd = cmd
    #target distance or angle to execute, max distance is 999 cm, and angle range is [0,180]
    msg.target = target
    #feedback of the msg is the interpolated average of the yaw angle before and after 'w' or 's' execution, to account for drifting
    #after executing turning 'a' or 'd', msg.feedback is the yaw angle after execution
    msg.feedback = fback
    #publish the msg to 'feedback' topic back to the master node
    pub.publish(msg)

def master():

    global xbee
    global ser
    #start serial port
    ser = serial.Serial(PORT, 9600)
    #connect to XBee on the serial port
    xbee = XBee(ser)

    rospy.init_node('tx_cmd', anonymous=True)

    #initial command is going straight at an drifted angle at fixed stepsize distance
    print("Press any key to start")

    raw_input()

    cmd = cmd_format('w', stepsize)

    #log infor using rospy, preparing to send via XBee to Fio
    rospy.loginfo("Sending command: %s" % cmd)
    #send via xbee
    xbee.tx(dest_addr_long = XBEE_ADDR_LONG, dest_addr = XBEE_ADDR_SHORT, data=cmd,)

    feedback_xbee()

    rospy.Subscriber('fb_cmd', Odom, callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':

    try:
        master()
    except KeyboardInterrupt or rospy.ROSInternalException:
        exit()

