import matplotlib.pyplot as plt
import numpy as np
import time



class DynamicUpdate():
    #Suppose we know the x range
    min_x = 0
    max_x = 10
    def __init__(self, traj):
        self.xdata = [0]
        self.ydata = [0]
        self.orient = 0
        if traj is not None:
            self.xref = traj[0]
            self.yref = traj[1]
        self.on_launch()

    def on_launch(self):
        #Set up plot
        self.figure, self.ax = plt.subplots()
        self.lines, = self.ax.plot([],[], 'r-', label='real')
        #Autoscale on unknown axis and known lims on the other
        self.ax.set_autoscaley_on(True)
        try:
            self.ax.set_xlim(min(self.xref)-5, max(self.xref)+5)
            self.ax.set_ylim(min(self.yref) - 5, max(self.yref) + 5)
            self.ax.plot(self.xref, self.yref, label='ref')
        except AttributeError:
            self.ax.set_xlim(self.min_x, self.max_x)
        #Other stuff
        self.ax.grid()
        self.ax.legend()


    def on_running(self):
        #Update data (with the new _and_ the old points)
        self.lines.set_xdata(self.xdata)
        self.lines.set_ydata(self.ydata)
        #Need both of these in order to rescale
        self.ax.relim()
        self.ax.autoscale_view()

        try:
            if min(self.xdata) < min(self.xref):
                self.ax.set_xlim(left=min(self.xdata)-5)
            if max(self.xdata) > max(self.xref):
                self.ax.set_xlim(right=max(self.xdata)+5)
            if min(self.ydata) < min(self.yref):
                self.ax.set_ylim(bottom=min(self.ydata)-5)
            if max(self.ydata) > max(self.yref):
                self.ax.set_ylim(top=max(self.ydata)+5)

        except AttributeError:
            self.ax.set_xlim(min(self.xdata)-5, max(self.xdata)+5)
            self.ax.set_ylim(min(self.ydata)-5, max(self.ydata)+5)
        #We need to draw *and* flush
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()

    def update(self, x, y):
        self.xdata.append(x)
        self.ydata.append(y)
        self.on_running()
        time.sleep(0.1)

if __name__ =="__main__":
    plt.ion()

    d = DynamicUpdate()

    while True:

        sth = raw_input()
        data = sth.split(" ")
        d.update(data[0], data[1])

    plt.ioff()
    plt.show()