#!/usr/bin/env python

import rospy
from std_msgs.msg import String
import serial
from xbee import XBee
import struct
from autocar.msg import Odom

xbee = None
# xbee address DH + DL concatenated
XBEE_ADDR_LONG = '\x00\x00\x00\x00\x00\x00\x00\x00'
# xbee 16-bit Source Address MY
XBEE_ADDR_SHORT = '\x00\x01'

# change your port here!
PORT = '/dev/ttyUSB0'

pub = rospy.Publisher('feedback', Odom, queue_size=1)

#once msg from the subscribed 'cmd' topic is received, callback is triggered to send the msg to Fio via XBee
#and then calls for feedback
def callback_xbee(msg1):
    #decode the received message
    tx_data = msg1.data.split("-")[0]
    #log infor using rospy, preparing to send via XBee to Fio
    rospy.loginfo("Sending keyboard command: %s" % tx_data)
    #send via xbee
    xbee.tx(dest_addr_long = XBEE_ADDR_LONG, dest_addr = XBEE_ADDR_SHORT, data=tx_data,)
    #call feedback_xbee() to wait for Fio's feedback reply
    feedback_xbee()

#waits for feedback from Fio via XBee, decode the feedback, and publish back to the master node
def feedback_xbee():
    #wait and read the reply from Fio
    response = xbee.wait_read_frame()
    #decode the feedback message
    cmd = response['rf_data'][0]
    target = struct.unpack('>h', response['rf_data'][1:3])[0]
    fback = struct.unpack('>h', response['rf_data'][-2:])[0]
    #declare message type, defined in Odom
    msg = Odom()
    #direction command, 'w','s','a', or 'd'
    msg.cmd = cmd
    #target distance or angle to execute, max distance is 999 cm, and angle range is [0,180]
    msg.target = target
    #feedback of the msg is the interpolated average of the yaw angle before and after 'w' or 's' execution, to account for drifting
    #after executing turning 'a' or 'd', msg.feedback is the yaw angle after execution
    msg.feedback = fback
    #publish the msg to 'feedback' topic back to the master node
    pub.publish(msg)


def slave():

    global xbee
    global ser
    #start serial port
    ser = serial.Serial(PORT, 9600)
    #connect to XBee on the serial port
    xbee = XBee(ser)
    #initiate 'slave' node
    rospy.init_node('slave', anonymous=True)
    #subscribe to the 'cmd' topic, which is published from the 'master' node
    rospy.Subscriber('cmd', String, callback_xbee)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    try:
        slave()
    except KeyboardInterrupt or rospy.ROSInternalException:
        #stop XBee
        xbee.halt()
        #close serial port
        ser.close()
