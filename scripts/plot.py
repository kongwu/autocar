#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String
from autocar.msg import Odom
from plot_real_time import DynamicUpdate
import matplotlib.pyplot as plt
import math
import time

x = 0
y = 0


def callback(msg, d):
    global x
    global y
    rospy.loginfo(msg)
    print(msg.cmd)
    print(msg.target)
    print(msg.feedback)
    if msg.cmd == 'w':
        x = x + msg.target * np.cos(math.radians(msg.feedback))
        y = y + msg.target * np.sin(math.radians(msg.feedback))
        print('x is now %f'%x)
        print('y is now %f'%y)
        d.update(x, y)
    if msg.cmd == 's':
        x = x - msg.target * np.cos(math.radians(msg.feedback))
        y = y - msg.target * np.sin(math.radians(msg.feedback))
        print('x is now %f'%x)
        print('y is now %f'%y)
        d.update(x, y)


def slave():

    rospy.init_node('plot', anonymous=True)

    plt.ion()

    d = DynamicUpdate(traj=None)

    rospy.Subscriber('feedback', Odom, callback, d)

    # spin() simply keeps python from exiting until this node is stopped

    plt.show(block=True)

if __name__ == '__main__':
    try:

        slave()
    except KeyboardInterrupt or rospy.ROSInternalException:
        exit()