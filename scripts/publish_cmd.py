#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from autocar.msg import Odom


def callback(data):
    rospy.loginfo(data)


def master():
    pub = rospy.Publisher('cmd', String, queue_size=1)
    #subscribe to 'feedback' topic, once the 'cmd' has been executed
    rospy.Subscriber('feedback', Odom, callback)
    rospy.init_node('master', anonymous=True)

    #while running taking input from keyboard and publish to the 'cmd'topic
    while not rospy.is_shutdown():
        #input format needs to be 'w','a','s', or'd' + 3 digits, i.e., w050 (going forward for 50cm), or d090 (turning right for 90 degrees)
        something = raw_input()
        #put on time stamp
        keyboard_str = something + "- %s" % rospy.get_time()
        #log the info using rospy
        rospy.loginfo(keyboard_str)
        #publish it to 'cmd' topic to the 'slave node'
        pub.publish(keyboard_str)


if __name__ == '__main__':

    try:
        master()
    except KeyboardInterrupt or rospy.ROSInternalException:
        exit()

