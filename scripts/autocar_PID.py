#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String
from autocar.msg import Odom
from dynamic_plot import DynamicUpdate
import matplotlib.pyplot as plt
import math


#start from origin
x = 0
y = 0

#PID gains and stepsize of driving forward

# kp = 8
# kd = 4
# ki = 0.005
# stepsize = 5

kp = 1.3
kd = 0.4
ki = 0.01
stepsize = 20
#publisher that publishes Odom type messages to 'fb_cmd' topic,
# which are the new command messages computed by the PID controller
pub = rospy.Publisher('fb_cmd', Odom, queue_size=100)

def pub_fb_cmd(steering, stepsize):

    fb_cmd = Odom()

    #if new steering computed by PID > 0, then turn left, and update the odometry as if turning has been executed
    if steering > 0:
        steering = math.ceil(steering)
        fb_cmd.cmd = 'a'
        fb_cmd.target = steering
        # print("computed turnining left for {}".format(fb_cmd.target))
    #if new steering computed by PID < 0, then turn right, and update the odometry as if turning has been executed
    elif steering < 0:
        steering = math.floor(steering)
        fb_cmd.cmd = 'd'
        fb_cmd.target = abs(steering)
        # print("computed turnining right for {}".format(fb_cmd.target))
    #if new steering is 0, go straight for the fixed stepsize in the direction of the last orientation
    else:
        fb_cmd.cmd = 'w'
        fb_cmd.target = stepsize
        # print("computed turing is zero, thus go straight")
    # print("computed PID command: ")
    # publish the new command to the emulator
    pub.publish(fb_cmd)
    # rospy.loginfo(fb_cmd)
    # print(" ")
    return 0



#rectify the orientation yaw angles in case goes out of range
def rectify_orient(orient):
    if orient > 180:
        orient = orient%180 - 180
    if orient < -180:
        orient = orient%180
    return orient

#execute the 'w' forward command and update odometry for plotting
def callback(msg, d):
    global x
    global y

    print("FEEDBACK FROM ARDUINO")
    rospy.loginfo(msg)

    last_orient = msg.feedback

    xref = d.get_traj()

    #check if exceedes the specified target line
    if d.get_x()[-1] < max(xref):
    #update the odometry and plotting after moving forward
    # if msg.cmd == 'w':
        if msg.cmd == 'w':
            x = x + msg.target * np.cos(math.radians(last_orient))
            y = y + msg.target * np.sin(math.radians(last_orient))
            print('went forward for {} cm with {} deg'.format(msg.target, last_orient))
            print('x is now %f'%x)
            print('y is now %f'%y)
            d.update_dist(x, y)

        # if msg.cmd == 's':
        #     x = x - msg.target * np.cos(math.radians(last_orient))
        #     y = y - msg.target * np.sin(math.radians(last_orient))
        #     print('went backward for {} cm with {} deg'.format(msg.target, last_orient))
        #     print('x is now %f'%x)
        #     print('y is now %f'%y)
        #     d.update_dist(x, y)
        #     orient = last_orient
        #     d.update_orient(orient)

            if len(d.get_y()) == 1:
                # cte = d.get_y()[-1]
                # df =  d.get_y()[-1]
                # CTE = d.get_y()[-1]
                cte = y
                df =  y
                CTE = y
            else:
                cte = d.get_y()[-1]
                df = d.get_y()[-2]
                CTE = np.sum(d.get_y())
            print("cte is: {}".format(cte))
            steering = rectify_orient(- kp * cte - kd * (cte - df) - ki * CTE)
            print("PID steering: {}".format(steering))
            print(" ")
            rospy.sleep(0.5)
            #publish the new command
            pub_fb_cmd(steering, stepsize)
            # else:
            #     print("default")

        elif msg.cmd == 'a' or 'd':
            #if 'a' or 's' turning execution has just been done, then go forward
            rospy.sleep(0.5)
            print("turning!")
            pub_fb_cmd(0, stepsize)
        else:
            print("do nothing")
    else:
        exit()

#target trajectory, a straight line on the x-axis for 200cm long
def ref_trajectory():
    xref = np.arange(0,200,1)
    yref = np.zeros(200)
    traj = [xref, yref]
    return traj

def slave():

    rospy.init_node('PID_controller', anonymous=True)

    plt.ion()

    traj = ref_trajectory()

    #set traj=None when there's no reference trajectory
    # d = DynamicUpdate(traj=None)
    d = DynamicUpdate(traj)

    rospy.Subscriber('feedback', Odom, callback, d)

    # block=True simply keeps python from exiting until this node is stopped
    plt.show(block=True)

if __name__ == '__main__':
    try:
        slave()
    except KeyboardInterrupt or rospy.ROSInternalException:
        exit()