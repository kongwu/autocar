#!/usr/bin/env python

import rospy
import numpy as np
from std_msgs.msg import String
from autocar.msg import Odom
from dynamic_plot import DynamicUpdate
import matplotlib.pyplot as plt
import math
import time

#start from origin
x = 0
y = 0

#PID gains and stepsize of driving forward

# kp = 8
# kd = 4
# ki = 0.005
# stepsize = 5

kp = 2
kd = 0.7
ki = 0.005
stepsize = 20
#publisher that publishes Odom type messages to 'fb_cmd' topic,
# which are the new command messages computed by the PID controller
pub = rospy.Publisher('fb_cmd', Odom, queue_size=1)

def pub_fb_cmd(steering, stepsize, last_orient, d):
    fb_cmd = Odom()
    steering = int (steering)
    #if new steering computed by PID > 0, then turn left, and update the odometry as if turning has been executed
    if steering > 0:


        fb_cmd.cmd = 'a'
        fb_cmd.target = steering
        fb_cmd.feedback = rectify_orient(steering + last_orient)
        print("computed turnining left for {} from {} to {} deg".format(steering, last_orient, fb_cmd.feedback))
        d.update_orient(fb_cmd.feedback)
    #if new steering computed by PID < 0, then turn right, and update the odometry as if turning has been executed
    elif steering < 0:
        fb_cmd.cmd = 'd'
        fb_cmd.target = abs(steering)
        fb_cmd.feedback = rectify_orient(steering + last_orient)
        print("computed turnining right for {} from {} to {} deg".format(steering, last_orient, fb_cmd.feedback))
        d.update_orient(fb_cmd.feedback)
    #if new steering is 0, go straight for the fixed stepsize in the direction of the last orientation
    else:
        fb_cmd.cmd = 'w'
        fb_cmd.target = stepsize
        fb_cmd.feedback = last_orient

    print("The feedback command now is: ")
    # rospy.loginfo(fb_cmd)

    #publish the new command to the emulator
    pub.publish(fb_cmd)

#rectify the orientation yaw angles in case goes out of range
def rectify_orient(orient):
    if orient > 180:
        orient = orient%180 - 180
    if orient < -180:
        orient = orient%180
    return orient

#execute the 'w' forward command and update odometry for plotting
def callback(msg, d):
    global x
    global y
    rospy.loginfo(msg)

    #if just started, get last orientation as the initalized 0 angle from DynamicUpdate()
    if len(d.get_orient()) == 1:
        last_orient = msg.feedback
    #otherwise get the lastest orientation stored there
    else:
        last_orient = d.get_orient()[-1]

    #update the odometry and plotting after moving forward
    if msg.cmd == 'w':
        x = x + msg.target * np.cos(math.radians(last_orient))
        y = y + msg.target * np.sin(math.radians(last_orient))
        print('went forward for {} cm with {} deg'.format(msg.target, last_orient))
        print('x is now %f'%x)
        print('y is now %f'%y)
        d.update_dist(x, y)
        orient = last_orient
        d.update_orient(orient)
    # if msg.cmd == 's':
    #     x = x - msg.target * np.cos(math.radians(last_orient))
    #     y = y - msg.target * np.sin(math.radians(last_orient))
    #     print('went backward for {} cm with {} deg'.format(msg.target, last_orient))
    #     print('x is now %f'%x)
    #     print('y is now %f'%y)
    #     d.update_dist(x, y)
    #     orient = last_orient
    #     d.update_orient(orient)

    xref = d.get_traj()
    #check if exceedes the specified target line
    if x < max(xref):
        #then apply PID for new steering after 'w' or 's' execution
        if msg.cmd == 'w' or 's':
            cte = y
            if len(d.get_y()) == 1:
                df =  y
                CTE = y
            else:
                df = d.get_y()[-2]
                CTE = np.sum(d.get_y())
            steering = - kp * cte - kd * (cte - df) - ki * CTE
            #publish the new command
            pub_fb_cmd(steering, stepsize, last_orient, d)
        else:
            #if 'a' or 's' turning execution has just been done, then go forward
            pub_fb_cmd(0, stepsize, orient, d)

    else:
        exit()

#target trajectory, a straight line on the x-axis for 200cm long
def ref_trajectory():
    xref = np.arange(0,400,1)
    yref = np.zeros(400)
    traj = [xref, yref]
    return traj

def slave():

    rospy.init_node('plot', anonymous=True)

    plt.ion()

    traj = ref_trajectory()

    #set traj=None when there's no reference trajectory
    # d = DynamicUpdate(traj=None)
    d = DynamicUpdate(traj)

    rospy.Subscriber('feedback', Odom, callback, d)

    # block=True simply keeps python from exiting until this node is stopped
    plt.show(block=True)

if __name__ == '__main__':
    try:
        slave()
    except KeyboardInterrupt or rospy.ROSInternalException:
        exit()